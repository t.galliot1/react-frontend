import React, { Component } from 'react';
import { Carousel, CarouselCaption, CarouselInner, CarouselItem, View, MDBBtn, MDBMask, MDBView, MDBContainer, MDBRow, MDBCol} from "mdbreact";
import './App.css';
import './components/donnée';

import ConfigCarousel from './components/ConfigCarousel';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      version:"",
      color:"",
      jante:"",
    }
    this.versionSelectedPure = this
        .versionSelectedPure
        .bind(this);
    this.versionSelectedLegende = this
        .versionSelectedLegende
        .bind(this);
    this.colorSelectedPureWhite = this
        .colorSelectedPureWhite
        .bind(this);
    this.colorSelectedPureBlue = this
        .colorSelectedPureBlue
        .bind(this);
    this.colorSelectedPureBlack = this
        .colorSelectedPureBlack
        .bind(this);
    this.colorSelectedLegendeWhite = this
        .colorSelectedLegendeWhite
        .bind(this);
    this.colorSelectedLegendeBlue = this
        .colorSelectedLegendeBlue
        .bind(this);
    this.colorSelectedLegendeBlack = this
        .colorSelectedLegendeBlack
        .bind(this);
    this.janteSelectedPureStandard = this
        .janteSelectedPureStandard
        .bind(this);
    this.janteSelectedPureSerac = this
        .janteSelectedPureSerac
        .bind(this);
    this.janteSelectedLegende = this
        .janteSelectedLegende
        .bind(this);

  }

  versionSelectedPure(){
    this.setState(state => ({version: "Pure", color: "White"}))
  }

  versionSelectedLegende(){
    this.setState(state => ({version: "Legende", color: "White"}))
  }

  colorSelectedPureWhite(){
    this.setState(state => ({color: "White"}))
  }  

  colorSelectedPureBlue(){
    this.setState(state => ({color: "Blue"}))
  }

  colorSelectedPureBlack(){
    this.setState(state => ({color: "Black"}))
  }

  colorSelectedLegendeWhite(){
    this.setState(state => ({color: "White"}))
  } 

  colorSelectedLegendeBlue(){
    this.setState(state => ({color: "Blue"}))
  }

  colorSelectedLegendeBlack(){
    this.setState(state => ({color: "Black"}))
  }

  janteSelectedPureStandard(){
    this.setState(state => ({jante: "Standard"}))
  }

  janteSelectedPureSerac(){
    this.setState(state => ({jante: "Serac"}))
  }

  janteSelectedLegende(){
    this.setState(state => ({jante: "Legende"}))
  }

  render() {
    return (
      <div className="App">
        {/* choix de la version Pure blanche jante standard */}
        {this.state.version === "Pure" &&
          <div>
            {/* // carousel pour choix de la couleur */}
            <div>
              <MDBContainer  className="version">
                <MDBRow>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/blanc.jpg")}
                            className="img-fluid"
                            alt="" />
                      <p id="titre">Teinte spéciale blanc Glacier</p>
                      <p>0€</p>
                      <MDBBtn onClick={this.colorSelectedPureWhite} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src= {require("./assets/configurateur/couleurs/selection/bleu.jpg")}
                            className="img-fluid"
                            alt="" onClick={this.colorSelectedPureBlue}/>
                      <p id="titre">Teinte spéciale Bleu Alpine</p>
                      <p>1 800€</p>
                      <MDBBtn onClick={this.colorSelectedPureBlue} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src= {require("./assets/configurateur/couleurs/selection/noir.jpg")}
                            className="img-fluid"
                            alt=""/>
                      <p id="titre">Teinte métallisée Noir Profont</p>
                      <p>840€</p>
                      <MDBBtn onClick={this.colorSelectedPureBlack} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </div>
              
           
    {/* choix des jantes</MDBBtn> */}
          {(this.state.jante === "" || this.state.jante === "Standard")  &&
            <div>
              
              <MDBContainer className="jante">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3">              
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-standard.jpg")} className="img-fluid choixJ"
                          alt=""/>
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedPureStandard} size="sm">Jante standard</MDBBtn>
                    </MDBView>
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-serac.jpg")} className="img-fluid choixJ"
                          alt=""/>
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedPureSerac} size="sm">Jante serac</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  
                  {this.state.color === "White" &&     
                  <MDBCol lg="9" className="mb-8 jante">
                    <img src={require("./assets/configurateur/jantes/vues/couleur-blanc_jante-standard (2).jpg")} className="img-fluid"
                        alt=""/>
                  </MDBCol>
                  }

                  {this.state.color ==="Blue" &&                 
                  <MDBCol lg="9" className="mb-8 jante">
                  <img src={require("./assets/configurateur/jantes/vues/couleur-bleu_jante-standard (3).jpg")} className="img-fluid"
                      alt=""/>
                  </MDBCol>
                  
                   }              
                  
                </MDBRow>
              </MDBContainer>           
            </div> 
            }
          </div>
        }

        {/* choix de la version Legende blanche jante standard */}
        {this.state.version === "Legende" &&
          <div>
            {/* // carousel pour choix de la couleur */}
            <div>
              <MDBContainer  className="version">
                <MDBRow>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/blanc.jpg")}
                            className="img-fluid"
                            alt=""/>
                      <p id="titre">Teinte spéciale blanc Glacier</p>
                      <p>0€</p>
                      <MDBBtn onClick={this.colorSelectedLegendeWhite} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/bleu.jpg")}
                            className="img-fluid"
                            alt=""/>
                      <p id="titre">Teinte spéciale Bleu Alpine</p>
                      <p>1800€</p>
                      <MDBBtn onClick={this.colorSelectedLegendeBlue} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol>
                    <MDBView hover zoom>
                      <img  src={require("./assets/configurateur/couleurs/selection/noir.jpg")}
                            className="img-fluid"
                            alt=""/>
                      <p id="titre">Teinte métallisée Noir Profont</p>
                      <p>840€</p>
                      <MDBBtn onClick={this.colorSelectedLegendeBlack} color="blue-grey">Selectioner</MDBBtn>
                    </MDBView>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </div>

{/* choix des jantes</MDBBtn> */}
            <div>
              <MDBContainer className="jante">
                <MDBRow>
                  <MDBCol lg="3" className="mb-3">              
                    <MDBView hover zoom>
                      <img src={require("./assets/configurateur/jantes/selection/jante-legende.jpg")} className="img-fluid choixJ"
                          alt=""/>
                      <MDBBtn color="blue-grey" onClick={this.janteSelectedLegende} size="sm">Jante légende</MDBBtn>
                    </MDBView>
                  </MDBCol>
                  <MDBCol lg="9" className="mb-8 jante">
                    <img src={require("./assets/configurateur/jantes/vues/couleur-blanc_jante-legende (2).jpg")} className="img-fluid"
                        alt=""/>
                  </MDBCol>
                </MDBRow>
              </MDBContainer>
            </div>
          </div>
        }
      </div>
    );
  }
}

export default App;
