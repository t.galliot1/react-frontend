export default {
    carouselModelSelected: [
        {
            path: "modele/selection",
            slides: [
                {
                    img: "legende.png",
                    title: "Legende",
                    price: "58 500€",
                    onclick: this.versionSelectedLegende
                },
                {
                    img: "pure.png",
                    title: "Pure",
                    price: "54 700€",
                    onclick: this.versionSelectedPure
                }
            ]
        }
    ],
    carouselChoiceVersionPureWhiteStandardRim: [
        {
            path: "/modele/pure/",
            title: "Pure blanche",
            price: "54 700€",
            img: [
                {
                    img: "modele_pure-couleur_blanche-jante_standard (1).jpg"
                },
                {
                    img: "modele_pure-couleur_blanche-jante_standard (2).jpg"
                },
                {
                    img: "modele_pure-couleur_blanche-jante_standard (3).jpg"
                },
                {
                    img: "modele_pure-couleur_blanche-jante_standard (4).jpg"
                }
            ]
        }
    ],
    carouselChoiceVersionPureBlueStandardRim: [
        {
            path: "/modele/pure/",
            title: "Pure bleu",
            price: "56 500€",
            img: [
                {
                    img: "modele_pure-couleur_bleu-jante_standard (1).jpg"
                },
                {
                    img: "modele_pure-couleur_bleu-jante_standard (2).jpg"
                },
                {
                    img: "modele_pure-couleur_bleu-jante_standard (3).jpg"
                },
                {
                    img: "modele_pure-couleur_bleu-jante_standard (4).jpg"
                }
            ]
        }
    ],
    carouselChoiceVersionPureBlackStandardRim: [
        {
            path: "/modele/pure/",
            title: "Pure noire",
            price: "55 540€",
            img: [
                {
                    img: "modele_pure-couleur_Black-jante_standard (1).jpg"
                },
                {
                    img: "modele_pure-couleur_Black-jante_standard (2).jpg"
                },
                {
                    img: "modele_pure-couleur_Black-jante_standard (3).jpg"
                },
                {
                    img: "modele_pure-couleur_Black-jante_standard (4).jpg"
                }
            ]
        }
    ],
    carouselChoiceVersionLegendeWhiteLegendeRim: [
        {
            path: "/modele/legende/",
            title: "Legende blanche",
            price: "58 500€",
            img: [
                {
                    img: "modele_legende-couleur_blanc-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_blanc-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_blanc-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_blanc-jante_legende-1.jpg"
                }
            ]
        }
    ],
    carouselChoiceVersionLegendeBlueLegendeRim: [
        {
            path: "/modele/legende/",
            title: "Legende bleu",
            price: "60 300€",
            img: [
                {
                    img: "modele_legende-couleur_bleu-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_bleu-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_bleu-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_bleu-jante_legende-1.jpg"
                }
            ]
        }
    ],
    carouselChoiceVersionLegendeBlackLegendeRim: [
        {
            path: "/modele/legende/",
            title: "Legende noire",
            price: "59 340€",
            img: [
                {
                    img: "modele_legende-couleur_noir-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_noir-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_noir-jante_legende-1.jpg"
                },
                {
                    img: "modele_legende-couleur_noir-jante_legende-1.jpg"
                }
            ]
        }
    ],
    choiceRim: [
        {
            path: "jantes/vues/",
            img: [
                {
                    img: "couleur-blanc_jante-legende (2).jpg"
                },
                {
                    img: "couleur-blanc_jante-serac (2).jpg"
                },
                {
                    img: "couleur-blanc_jante-standard (2).jpg"
                },
                {
                    img: "couleur-bleu_jante-legende (3).jpg"
                },
                {
                    img: "couleur-bleu_jante-serac (3).jpg"
                },
                {
                    img: "couleur-bleu_jante-standard (3).jpg"
                },
                {
                    img: "couleur-noir_jante-legende (1).jpg"
                },
                {
                    img: "couleur-noir_jante-serac (1).jpg"
                },
                {
                    img: "couleur-noir_jante-standard (1).jpg"
                }
            ]
        }
    ]
}